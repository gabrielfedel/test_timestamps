import epics
import h5py
import datetime
import time

PVlist = []
prefix = "test"
PVPREFIX = "DTL-050:RFS-DIG-101:"

PVlist.append(PVPREFIX +'Dwn'+str('0')+'-Cmp0')
PVlist.append(PVPREFIX +'Dwn'+str('0')+'-Cmp1')
PVlist.append(PVPREFIX +'Dwn'+str('5')+'-Cmp0')
PVlist.append(PVPREFIX +'Dwn'+str('5')+'-Cmp1')
PVlist.append(PVPREFIX +'Dwn'+str('6')+'-Cmp0')
PVlist.append(PVPREFIX +'Dwn'+str('6')+'-Cmp1')

#PVlist.append('LLRF:DIG1'+':Dwn0-XAxis')

now = datetime.datetime.now(tz=datetime.timezone(datetime.timedelta(hours=2))).isoformat()
hdf5Filename = 'test-data_'+str(prefix) + '_' +now+'.hdf5'
hdf5File = h5py.File(hdf5Filename, 'w')


def saver(**kws):
    try:
    #print('Saving data for PV '+kws['pvname'])
        hdf5File[kws['pvname']].create_dataset(str(kws['timestamp']),
                                               data = kws['value'])
    except:
        print("err")
#    PVs[kws['pvname']].auto_monitor = False

PVs = {}
for pv in PVlist:
    hdf5File.create_group(pv)
    PVs[pv] = epics.get_pv(pv, auto_monitor=False, callback=saver)

for pv in PVs:
    PVs[pv].auto_monitor=True

time.sleep(5)

for pv in PVs:
    PVs[pv].auto_monitor=False

hdf5File.close()

## finishe creating the file

## Begin to read the file

hdf5File = h5py.File(hdf5Filename, 'r')

key_group = []
for key in hdf5File.keys():
    key_group.append(key)

print(key_group)

#gr = hdf5File.get('LLRF:DIG1:Dwn0-Cmp0')

#print(gr.keys())
ts = []

#for k in key_group:
#    for c in hdf5File.get(k):
#        ts.append(float(c))
#
#    for i in range(len(ts)):
#        if i > 0:
#            if (ts[i] - ts[i-1]) > 0.072:
#                print ("Invalid!")
#    ts = []
#
#print("Everything run fine!")

# Print the difference between the timestamps
for c in hdf5File.get(PVPREFIX +'Dwn'+str('0')+'-Cmp0'):
    ts.append(float(c))


for i in range(len(ts)):
    if i > 0:
        print(ts[i] - ts[i-1])

